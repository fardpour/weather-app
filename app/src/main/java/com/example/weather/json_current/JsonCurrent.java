
package com.example.weather.json_current;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.weather.Util.Helper;

import java.util.List;


public class JsonCurrent extends BaseObservable {


    @Bindable
    private Coord coord;

    @Bindable
    private Main main;

    @Bindable
    private String name;

    @Bindable
    private long timezone;

    @Bindable
    private List<Weather> weather;



    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }


    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimezone() {
        return timezone;
    }

    public void setTimezone(long timezone) {
        this.timezone = timezone;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }
}
