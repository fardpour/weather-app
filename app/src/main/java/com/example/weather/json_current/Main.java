
package com.example.weather.json_current;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.google.gson.annotations.SerializedName;

public class Main extends BaseObservable {

    @Bindable
    @SerializedName("temp")
    private double temp;

    @Bindable
    @SerializedName("temp_max")
    private double tempMax;

    @Bindable
    @SerializedName("temp_min")
    private double tempMin;

    public String getTemp() {
        return (int)temp+"\u00B0";
    }

    public void setTemp(double temp) {
        this.temp = temp;
        notifyPropertyChanged(BR.temp);
    }

    public String getTempMax() {
        return (int)tempMax+"\u00B0";
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
        notifyPropertyChanged(BR.max);
    }

    public String getTempMin() {
        return (int)tempMin+"\u00B0";
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
        notifyPropertyChanged(BR.min);
    }
}
