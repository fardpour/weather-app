package com.example.weather.json_data_model;


import android.widget.ImageView;

import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.weather.BR;
import com.google.gson.annotations.SerializedName;

public class Weather extends BaseObservable {
    @Bindable
    @SerializedName("description")
    private String description;

    @Bindable
    @SerializedName("id")
    private int id;

    @Bindable
    @SerializedName("main")
    private String main;

    @Bindable
    private int weatherIcon;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }


    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
        notifyPropertyChanged(BR.main);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public int getIcon() {
        return weatherIcon;
    }

    public void setIcon(int icon) {
        this.weatherIcon = icon;
        notifyPropertyChanged(BR.weatherIcon);
    }

    @BindingAdapter("image")
    public static void loadImage(ImageView view, int imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .into(view);
    }
}
