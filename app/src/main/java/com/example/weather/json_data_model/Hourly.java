package com.example.weather.json_data_model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.weather.BR;
import com.example.weather.Util.Helper;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Hourly extends BaseObservable {

    @Bindable
    @SerializedName("dt")
    private long dt;

    @Bindable
    @SerializedName("temp")
    private double temp;

    @Bindable
    @SerializedName("weather")
    private List<Weather> weather;


    public String getTemp() {
        return temp+"\u00B0";
    }

    public void setTemp(double temp) {
        this.temp = temp;
        notifyPropertyChanged(BR.temp);
    }


    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
        notifyPropertyChanged(BR.weather);
    }

    public String getDt() {
        return Helper.getHour(this.dt);

    }

    public void setDt(long dt) {
        this.dt = dt;
        notifyPropertyChanged(BR.dt);
    }
}
