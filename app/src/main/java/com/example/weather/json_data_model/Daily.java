package com.example.weather.json_data_model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.weather.BR;

import java.util.List;

public class Daily extends BaseObservable {

    @Bindable
    private String dayName;

    @Bindable
    private long dt;

    @Bindable
    private Temp temp;

    @Bindable
    private List<Weather> weather;



    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
        notifyPropertyChanged(BR.dt);
    }

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
        notifyPropertyChanged(BR.temp);
    }


    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
        notifyPropertyChanged(BR.weather);
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
        notifyPropertyChanged(BR.dayName);
    }
}
