package com.example.weather.json_data_model;


import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.weather.BR;

public class Temp extends BaseObservable {

    @Bindable
    private double max;
    @Bindable
    private double min;


    public int getMax() {
        return (int)max;
    }

    public void setMax(double max) {
        this.max = max;
        notifyPropertyChanged(BR.max);
    }

    public int getMin() {
        return (int)min;
    }

    public void setMin(double min) {
        this.min = min;
        notifyPropertyChanged(BR.min);
    }


}
