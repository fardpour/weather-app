package com.example.weather.ViewModelFactory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.weather.Interface.IGetData;
import com.example.weather.model.MainActivityViewModel;

public class MainViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private IGetData iGetData;

    public MainViewModelFactory(IGetData iGetData) {
        this.iGetData = iGetData;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainActivityViewModel(iGetData);
    }



}
