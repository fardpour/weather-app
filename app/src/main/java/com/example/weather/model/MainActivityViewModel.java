package com.example.weather.model;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.weather.ApiService.RetrofitSingleton;
import com.example.weather.Interface.ApiServices;
import com.example.weather.Interface.IGetData;
import com.example.weather.Util.Helper;
import com.example.weather.json_current.JsonCurrent;
import com.example.weather.json_data_model.Hourly;
import com.example.weather.json_data_model.JsonDays;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivityViewModel extends ViewModel {
    private static final String TAG = "MainActivityViewModel";

    ApiServices apiServices = RetrofitSingleton.getInstance();
    private String appKey = "266a45fc07cf25625a8cbab2c52f6d73";
    private IGetData iGetData;

    private MutableLiveData<JsonDays> liveDataForCast;

    private MutableLiveData<JsonCurrent> liveDataCurrent;

    public MutableLiveData<JsonDays> getLiveDataForCast() {
        return liveDataForCast;
    }

    public MutableLiveData<JsonCurrent> getLiveDataCurrent() {
        return liveDataCurrent;
    }

    private CompositeDisposable disposable = new CompositeDisposable();

    public MainActivityViewModel(IGetData iGetData) {
        this.iGetData = iGetData;
    }

    public MutableLiveData<JsonDays> getDataForCast(double lon, double lat) {
        if (liveDataForCast == null) {
            liveDataForCast = new MutableLiveData<>();
        }
        disposable.add(apiServices.getData(lon, lat, "minutely,current", appKey, "metric")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonDays>() {
                    @Override
                    public void onSuccess(JsonDays jsonDays) {
                        Log.i(TAG, "onSuccess: " + jsonDays.getTimezone());
                        List<Hourly> hoursList = jsonDays.getHourly().subList(0, 24);
                        jsonDays.setHourly(hoursList);


                        jsonDays.getDaily().get(0).setDayName("Today");
                        jsonDays.getDaily().get(1).setDayName("Tomorrow");
                        for (int i = 0; i < jsonDays.getHourly().size(); i++) {
                            jsonDays.getHourly().get(i).getWeather().get(0).setIcon(
                                    Helper.getDrawableId(jsonDays.getHourly().get(i).getWeather().get(0).getDescription())
                            );

                        }


                        for (int i = 0; i < jsonDays.getDaily().size(); i++) {
                            jsonDays.getDaily().get(i).getWeather().get(0).setIcon(
                                    Helper.getDrawableId(jsonDays.getDaily().get(i).getWeather().get(0).getDescription())
                            );

                            if (i > 1) {
                                jsonDays.getDaily().get(i).setDayName(Helper.convertTimestampToDay(
                                        jsonDays.getDaily().get(i).getDt()));
                            }

                        }


                        liveDataForCast.postValue(jsonDays);

                        Log.i(TAG, "onSuccess for cast: " + jsonDays.getHourly().get(jsonDays.getHourly().size() - 1).toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        iGetData.onError(e.getMessage());
                    }
                })
        );

        Log.i(TAG, "return mutable data " + liveDataForCast);
        return liveDataForCast;

    }

    public MutableLiveData<JsonCurrent> getDataForCurrent(String cityName) {
        if (liveDataCurrent == null) {
            liveDataCurrent = new MutableLiveData<>();
        }
        disposable.add(apiServices.getCurrent(cityName + ",IR", appKey, "metric")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonCurrent>() {
                    @Override
                    public void onSuccess(JsonCurrent jsonDays) {


                            jsonDays.getWeather().get(0).setImage(
                                    Helper.getDrawableId(jsonDays.getWeather().get(0).getDescription())
                            );



                        liveDataCurrent.postValue(jsonDays);
                        Log.i(TAG, "onSuccess for current : " + jsonDays.getName());
                    }

                    @Override
                    public void onError(Throwable e) {
                        iGetData.onError(e.getMessage());
                    }
                })
        );

        Log.i(TAG, "return mutable data " + liveDataForCast);
        return liveDataCurrent;
    }

    public void onClickCity(View view){
        iGetData.onError("onClick");
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (!disposable.isDisposed()) {
            disposable.isDisposed();
        }
    }
}
