package com.example.weather;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.weather.Adapter.AdapterRVDaily;
import com.example.weather.Adapter.AdapterRVHourly;
import com.example.weather.Interface.IAdapterClick;
import com.example.weather.Interface.IGetData;
import com.example.weather.Util.Helper;
import com.example.weather.ViewModelFactory.MainViewModelFactory;
import com.example.weather.databinding.ActivityMainBinding;
import com.example.weather.json_data_model.Daily;
import com.example.weather.model.MainActivityViewModel;

public class MainActivity extends AppCompatActivity implements IGetData, IAdapterClick {
    private static final String TAG = "MainActivity";

    private RecyclerView recyclerViewDaily, recyclerViewHourly;
    private AdapterRVDaily adapterDaily;
    private AdapterRVHourly adapterHourly;
    private MainActivityViewModel viewModel;
    private ActivityMainBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this,
                new MainViewModelFactory(this)).get(MainActivityViewModel.class);
        dataBinding.setViewModel(viewModel);

        Drawable drawable = ResourcesCompat.getDrawable(getResources(),
                Helper.getBackgroundImage(), null);
        dataBinding.getRoot().setBackground(drawable);


        viewModel.getDataForCurrent("Tehran").observe(MainActivity.this, response -> {

            dataBinding.acMainTvCityname.setText(response.getName());
            dataBinding.acMainTvDayname.setText("Today");
            dataBinding.acMainTvMaxTemp.setText(response.getMain().getTempMax() + "/" + response.getMain().getTempMin());
            dataBinding.acMainTvTemp.setText(response.getMain().getTemp());
            Glide.with(MainActivity.this).load(response.getWeather().get(0).getImage()).into(dataBinding.acMainImgvWeather);

            viewModel.getDataForCast(response.getCoord().getLon(), response.getCoord().getLat()).observe(MainActivity.this, serverResponse -> {
                adapterDaily.addDate(serverResponse.getDaily());
                adapterHourly.addData(serverResponse.getHourly());
            });
        });

        setupDailyRecyclerView();
        setupHourlyRecyclerView();


    }

    private void setupDailyRecyclerView() {
        recyclerViewDaily = dataBinding.acMainRvDaily;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewDaily.setLayoutManager(layoutManager);
        adapterDaily = new AdapterRVDaily(this, this);
        recyclerViewDaily.setAdapter(adapterDaily);
    }

    private void setupHourlyRecyclerView() {
        recyclerViewHourly = dataBinding.acMainRvHourly;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerViewHourly.setLayoutManager(layoutManager);
        adapterHourly = new AdapterRVHourly(this);
        recyclerViewHourly.setAdapter(adapterHourly);
    }

    private void showCustomDialog() {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("SEARCH CITY");

        // Setting Dialog Message
        alertDialog.setMessage("please type city");
        final EditText input = new EditText(MainActivity.this);
        alertDialog.setView(input);


        // Setting Positive "search" Button
        alertDialog.setPositiveButton("search",
                (dialog, which) -> {
                    if (!input.getText().toString().trim().isEmpty()) {
                        viewModel.getDataForCurrent(input.getText().toString()).observe(MainActivity.this, response -> {

                            dataBinding.acMainTvCityname.setText(response.getName());
                            dataBinding.acMainTvDayname.setText("Today");
                            dataBinding.acMainTvMaxTemp.setText(response.getMain().getTempMax() + "/" + response.getMain().getTempMin());
                            dataBinding.acMainTvTemp.setText(response.getMain().getTemp());
                            Glide.with(MainActivity.this).load(response.getWeather().get(0).getImage()).into(dataBinding.acMainImgvWeather);

                            viewModel.getDataForCast(response.getCoord().getLon(), response.getCoord().getLat()).observe(MainActivity.this, serverResponse -> {
                                adapterDaily.addDate(serverResponse.getDaily());
                                adapterHourly.addData(serverResponse.getHourly());
                            });
                        });

                    } else Toast.makeText(this, "please insert city", Toast.LENGTH_SHORT).show();
                });
        // Setting Negative "cancel" Button
        alertDialog.setNegativeButton("cancel",
                (dialog, which) -> dialog.cancel());

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onError(String message) {
        if (message.equals("onClick")) {
            showCustomDialog();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "onError: " + message);
        }
    }

    @Override
    public void iClick(int position) {
        viewModel.getLiveDataForCast().observe(MainActivity.this, jsonDays -> {
            Daily daily = jsonDays.getDaily().get(position);
            dataBinding.acMainTvDayname.setText(daily.getDayName());
            dataBinding.acMainTvMaxTemp.setText(daily.getTemp().getMax() + "\u00B0" + "/" + daily.getTemp().getMin() + "\u00B0");
            dataBinding.acMainTvTemp.setText(daily.getTemp().getMax() + "\u00B0");
            Glide.with(MainActivity.this).load(daily.getWeather().get(0).getIcon()).into(dataBinding.acMainImgvWeather);
        });
    }
}