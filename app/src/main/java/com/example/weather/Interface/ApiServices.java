package com.example.weather.Interface;

import com.example.weather.json_current.JsonCurrent;
import com.example.weather.json_data_model.JsonDays;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET("onecall?")
    Single<JsonDays> getData(@Query("lon") double lon,
                             @Query("lat") double lat,
                             @Query("exclude") String exclude,
                             @Query("appid") String appKey,
                             @Query("units") String metric
    );

    @GET("weather?")
    Single<JsonCurrent> getCurrent(
            @Query("q") String city,
            @Query("appid") String appKey,
            @Query("units") String metric
    );
}
