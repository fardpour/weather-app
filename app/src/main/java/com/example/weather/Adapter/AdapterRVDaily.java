package com.example.weather.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather.Interface.IAdapterClick;
import com.example.weather.databinding.SampleRvDailyBinding;
import com.example.weather.json_data_model.Daily;

import java.util.ArrayList;
import java.util.List;

public class AdapterRVDaily extends RecyclerView.Adapter<AdapterRVDaily.AdapterRVDailyViewHolder> {
    private Context context;
    private IAdapterClick iAdapterClick;
    private List<Daily> list = new ArrayList<>();

    public AdapterRVDaily(Context context, IAdapterClick iAdapterClick) {
        this.context = context;
        this.iAdapterClick = iAdapterClick;
    }

    public void addDate(List<Daily> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterRVDailyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterRVDailyViewHolder(SampleRvDailyBinding.inflate(LayoutInflater.from(context), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRVDailyViewHolder holder, int position) {
        holder.bindData(list.get(position));
        holder.itemView.setOnClickListener(v -> iAdapterClick.iClick(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AdapterRVDailyViewHolder extends RecyclerView.ViewHolder {
        private SampleRvDailyBinding binding;

        public AdapterRVDailyViewHolder(@NonNull SampleRvDailyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(Daily model) {
            binding.setViewModel(model);
            binding.executePendingBindings();
        }
    }
}
