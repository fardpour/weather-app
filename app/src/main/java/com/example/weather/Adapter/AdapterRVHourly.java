package com.example.weather.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather.databinding.SampleRvHourlyBinding;
import com.example.weather.json_data_model.Hourly;

import java.util.ArrayList;
import java.util.List;

public class AdapterRVHourly extends RecyclerView.Adapter<AdapterRVHourly.AdapterRVHourlyViewHolder> {

    private Context context;
    private List<Hourly> list = new ArrayList<>();

    public AdapterRVHourly(Context context) {
        this.context = context;
    }

    public void addData(List<Hourly> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterRVHourlyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterRVHourlyViewHolder(
                SampleRvHourlyBinding.inflate(
                        LayoutInflater.from(context), parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRVHourlyViewHolder holder, int position) {
        holder.setBinding(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AdapterRVHourlyViewHolder extends RecyclerView.ViewHolder {
        private SampleRvHourlyBinding binding;

        public AdapterRVHourlyViewHolder(@NonNull SampleRvHourlyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void setBinding(Hourly hourly) {
            binding.setViewModel(hourly);
            binding.executePendingBindings();
        }
    }
}
