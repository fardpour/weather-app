package com.example.weather.Util;

import android.util.Log;

import com.example.weather.R;

import java.util.Calendar;

public class Helper {
    private static final String TAG = "Helper";

    public static enum getEnum {
        gradDawn,
        gradMorning,
        gradNoon,
        gradEvening,
        gradNight
    }

    public static int getDrawable(getEnum getEnum) {
        switch (getEnum) {
            case gradDawn:
                return R.drawable.grad_dawn;
            case gradNoon:
                return R.drawable.grad_noon;
            case gradNight:
                return R.drawable.grad_night;
            case gradEvening:
                return R.drawable.grad_evening;
            case gradMorning:
                return R.drawable.grad_morning;
            default:
                return 0;
        }
    }

    public static int getBackgroundImage() {
        int time = getTime();

        if (time >= 0 && time <= 6) {
            return getDrawable(getEnum.gradDawn);
        } else if (time >= 7 && time <= 11) {
            return getDrawable(getEnum.gradMorning);
        } else if (time >= 12 && time <= 16) {
            return getDrawable(getEnum.gradNoon);
        } else if (time >= 17 && time <= 19) {
            return getDrawable(getEnum.gradEvening);
        } else if (time >= 20 && time <= 23) {
            return getDrawable(getEnum.gradNight);
        } else return getDrawable(getEnum.gradDawn);
    }

    public static int getTime() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static int getDrawableId(String description) {
        int time = getTime();
        switch (description) {
            case "scattered clouds":
            case "overcast clouds" :
                return R.drawable.ic_0_scattered_clouds;
            case "clear sky":
                if (time >= 0 && time <= 6) {
                    return R.drawable.ic_0_clear_sky_dawn;
                } else if (time >= 7 && time <= 16) {
                    return R.drawable.ic_0_clear_sky_morning;
                } else if (time >= 17 && time <= 19) {
                    return R.drawable.ic_0_clear_sky_evening;
                } else if (time >= 20 && time <= 23) {
                    return R.drawable.ic_0_clear_sky_night;
                }
            case "few clouds":
                if (time >= 0 && time <= 6) {
                    return R.drawable.ic_0_few_cloud_dawn;
                } else if (time >= 7 && time <= 16) {
                    return R.drawable.ic_0_few_cloud_morning;
                } else if (time >= 17 && time <= 19) {
                    return R.drawable.ic_0_few_cloud_evening;
                } else if (time >= 20 && time <= 23) {
                    return R.drawable.ic_0_few_clound_night;
                }
            case "broken clouds":
                return R.drawable.ic_0_broken_cloud;
            case "shower rain":
                return R.drawable.ic_0_shower_rain;
            case "rain":
            case "light rain":
                return R.drawable.ic_0_rain;
            case "thunderstorm":
                return R.drawable.ic_0__thunderstorm;
            case "snow":
                return R.drawable.ic_0_snow;
            case "mist":
                return R.drawable.ic_0_mist;
            default:
                return 0;
        }
    }

    public static String convertTimestampToDay(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis((timestamp * 1000L));
        int day = c.get(Calendar.DAY_OF_WEEK);
        Log.i(TAG, "convert timestamp to day : " + day);//between 1 to 7
        switch (day) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            case 7:
                return "Saturday";
            default:
                return "today";
        }

    }

    public static String getHour(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis((timestamp * 1000L));
        return c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
    }

}
